"""Pandas utilities"""
import difflib
import json
import re
import pandas
import requests
import unidecode as unidecode
from sqlalchemy import create_engine

from bioinformatica.settings import DBPASS


class Analysis:
    """Uma análise para cada arquivo"""
    def __init__(self):
        self.df = None
        self.dict_varkeys = None
        self.original_colnames = None

    @staticmethod
    def get_engine():
        return create_engine(f'postgresql://postgres:{DBPASS}@localhost/bioinformatica', encoding="utf-8")

    def set_df_from_file(self, filepath, filetype):
        if 'officedocument' in filetype.lower():
            self.df = self.df_from_excel(filepath)
        else:
            self.df = self.df_from_csv(filepath)

    def set_df_from_db(self, schema, table):
        engine = self.get_engine()
        query = f"select * from {schema}.{table}"
        self.df = pandas.read_sql(query, engine)

    def df_from_excel(self, filepath):
        df = pandas.read_excel(filepath)
        self.df = df
        return df

    def df_from_csv(self, csvfile):
        df = pandas.read_csv(csvfile)
        self.df = df
        return df

    def dedup(self, tablename, schema, colnames, keys):

        if len(keys) == 0: # não precisa fazer deduplicação
            return

        # agrega linhas com base na identidade das chaves
        engine = self.get_engine()
        query = f"select * from {schema}.{tablename}"
        df = pandas.read_sql(query, engine, columns=colnames)

        # Para descobrir a linha que tem mais elementos eu criei uma coluna chamada "maxfilled"
        # que contém o número de células preenchidas. Ao escolher linhas duplicadas, capturar
        # a que tem maxfilled maior
        maxfilled = "maxfilled"
        df[maxfilled] = df.isnull().sum(axis=1)
        df = df.sort_values(maxfilled).drop_duplicates(keys).reset_index(drop=True)

        # Remover a coluna maxfilled
        df = df.drop(columns=[maxfilled])

        df.to_sql(tablename + "_dedup", con=engine, schema=schema, if_exists="replace", index=False)
        self.df = df

    @staticmethod
    def remove_accents(a):
        if str(a) == 'nan' or str(a) == 'None':
            return a
        a = a.replace("'", "").replace('"', '')
        return unidecode.unidecode(a)

    def getvarname(self, x):
        result = self.remove_accents(re.sub('[\W_]+', '', x))
        return result.lower()

    def dict_from_df(self):
        return pandas.DataFrame.to_dict(self.df)

    def analysis_int(self, colname):
        dfcolumn = self.df[colname]
        return dfcolumn.describe()

    def getsummary(self, columname):
        "sumario para todas as variáreis exceto numéricas e datas"
        pass

    def getsummarynumber(self, columname):
        pass

    def getsummarydate(self, columname):
        pass

    def df_from_tuple(self, data, columns):
        df = pandas.DataFrame(data, columns=columns)
        self.df = df

    def texto(self, colname, **kwargs):
        self.df[colname] = self.df[colname].astype(str)

    def data(self, colname, **kwargs):
        date_format = kwargs.get("format")

        # todo implementar
        date_format = "%d-%m-%Y"

        self.df[colname] = pandas.to_datetime(self.df[colname], errors="coerce", format=date_format)

    def sexo(self, colname, **kwargs):
        # 0 = F, 1 = M
        m = ["m", "masc", "masculino", "masculina", "male", "homem", "man", "men", "1"]
        f = ["f", "fem", "feminino", "feminina", "female", "mulher", "woman", "women", "0"]
        self.df[colname] = self.df[colname].str.lower().replace(" ", "")
        dict_m = dict(zip(m, len(m)*["M"]))
        dict_f = dict(zip(f, len(f)*["F"]))
        replace_map = dict_m
        replace_map.update(dict_f)
        self.df[colname] = self.df[colname].replace(replace_map)
        # O restante fica vazio
        self.df[colname] = self.df[colname].apply(lambda x: "" if x != 'F' and x != "M" else x)

    @staticmethod
    def getsimilar(value, allvalues):
        if value is None:
            return "NA"
        value = str(value).capitalize()
        possibilidades = difflib.get_close_matches(value, allvalues)
        if len(possibilidades) > 0:
            found = possibilidades[0]
            return found
        else:
            return  "NA"

    def cidadebrasileira(self, colname, **kwargs):
        estado = kwargs.get("estado") # se o estado não é Nulo, significa que o estudo se passou em somente um estado
        if estado:
            url = f"https://servicodados.ibge.gov.br/api/v1/localidades/estados/{estado}/municipios"
            response = requests.get(url)
            lista_de_localidades = [(x["nome"], x["microrregiao"]["mesorregiao"]["nome"]) for x in json.loads(response.text)]
            cidades = [x[0] for x in lista_de_localidades]
            mesorregioes = [x[1] for x in lista_de_localidades]
            dict_mesorregioes = dict(zip(cidades, mesorregioes))
            self.df[colname] = self.df[colname].apply(self.getsimilar, args=[cidades])
            self.df["mesoregion"] = self.df[colname].apply(lambda x: dict_mesorregioes.get(x, ""))
        else:
            'pegar coluna de estados para encontrar a correlação: ainda não implementado'
        return

    @staticmethod
    def onlydigit(value):
        value = str(value)
        return "".join([x for x in value if x.isdigit()])


    def numerosinteiros(self, colname, **kwargs):
        # mantem somente os digitos
        self.df[colname] = self.df[colname].apply(self.onlydigit)
        self.df[colname] = pandas.to_numeric(self.df[colname], downcast="integer", errors="coerce")

    def numerosreais(self, colname, **kwargs):
        # remove espaços
        self.df[colname] = self.df[colname].apply(lambda x:str(x).replace(" ", ""))
        # substituir vírgulas por pontos
        self.df[colname] = self.df[colname].apply(lambda x:x.replace(",", "."))
        self.df[colname] = pandas.to_numeric(self.df[colname], downcast="float", errors="coerce")



    def fixtypes(self, types, table, schema, **kwargs):
        for k,v in types.items():
            try:
                func = unidecode.unidecode(v.lower()).replace(" ", "")
                getattr(self, func)(k, **kwargs)
            except AttributeError:
                getattr(self, "texto")
        engine = self.get_engine()
        self.df.to_sql(table + "_typefixed", engine, schema=schema, if_exists='replace', index=False)


    def persist(self, schema, tablename):
        """Persiste o dataframe e retorna o nome das variáveis"""
        original_colnames = list(self.df.columns)
        keys = [x for x in original_colnames if "nnamed" not in x] # remove unnamed dos arquivos excel
        self.original_colnames = keys
        varkeys = [self.getvarname(x) for x in keys]
        dfraw = self.df[keys]
        self.dict_varkeys  = dict(zip(keys, varkeys))
        dfraw = dfraw.rename(columns=self.dict_varkeys )
        engine = self.get_engine()
        dfraw.to_sql(tablename, con=engine, schema=schema, if_exists="replace", index=False)
        return varkeys, keys



