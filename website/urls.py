from django.contrib import admin
from django.urls import path, include
from website.views import *

urlpatterns = [
    path('', home, name='home_view'),
    path('pesquisador/', PesquisadorView, name='bio'),
    path('datascience/', DSView, name='dsview'),
    path('datascience/d3/', D3View, name='d3view'),
    path('dataengineer/', DEView, name='deview'),
    path('softwaredevelopment/', SDView, name='sdview'),
    path('cv/', CVView, name='cvview'),
    path('download/<path>/', download, name='download'),
]

