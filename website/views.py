"""
Se precisar excluir uma tabela, execute
python manage.py migrate --fake website zero
python manage.py migrate website
"""

import datetime
import logging
import os
import shutil
import secrets
import magic  # descobre os tipos de arquivos
import numpy

from django.contrib.auth.decorators import login_required
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage
from django.db import connections
from django.http import HttpResponse, Http404
from django.shortcuts import render, redirect
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from numpy.random import seed

from bioinformatica import settings
from plots import PlotBio
from website.analysis import Analysis
from django.core.files.storage import default_storage

from website.models import Arquivos

from website.models import Trabalhos
from website.tables import AssistenteTable


WORKDIR = settings.BASE_DIR


@login_required(redirect_field_name='login')
def home(request):
    # Verifica se o schema do usuario existe, senão cria
    # try:
    #     var = request.user.schema  # TODO Verifica um modo melhor de salvar o schema sem ficar repetindo
    #     logging.info(f"schema já existe: {var}")
    # except AttributeError:
    #     with connections['default'].cursor() as cursor:
    #         username = request.user.username
    #         query = f"CREATE SCHEMA IF NOT EXISTS {username};"
    #         cursor.execute(query)
    #         request.user.schema = username
    #         logging.info(f"schema não existia e foi criado: {request.user.schema}")
    return render(request, 'home.html', {})


@login_required(redirect_field_name='login')
def PesquisadorView(request):
    context = {}
    return render(request, 'pesquisador.html', context)



@login_required(redirect_field_name='login')
def DSView(request):

    rand = numpy.random.RandomState(7)
    values = list(rand.exponential(8, 500))
    plot = PlotBio(values)
    graph = plot.dist_number()
    context = {"graph": graph}
    return render(request, 'ds.html', context)


@login_required(redirect_field_name='login')
def DEView(request):
    context = {}
    return render(request, 'de.html', context)


@login_required(redirect_field_name='login')
def SDView(request):
    context = {}
    return render(request, 'sd.html', context)


@login_required(redirect_field_name='login')
def CVView(request):
    context = {}
    return render(request, 'cv.html', context)


@login_required(redirect_field_name='login')
def D3View(request):
    context = {}
    return render(request, 'd3.html', context)


def download(request, path):
    file_path = os.path.join(settings.MEDIA_ROOT, path + ".pdf")
    if os.path.exists(file_path):
        with open(file_path, 'rb') as fh:
            response = HttpResponse(fh.read(), content_type="application/vnd.ms-excel")
            response['Content-Disposition'] = 'inline; filename=' + os.path.basename(file_path)
            return response
    raise Http404


#
# @login_required(redirect_field_name='login')
# def ShinyView(request):
#     context = {}
#     return render(request, 'omica/home.html', context)
#
# @login_required(redirect_field_name='login')
# def D3View(request):
#     context = {}
#     return render(request, 'omica/home.html', context)
#
#
# @login_required(redirect_field_name='login')
# def PrecosView(request):
#     context = {}
#     return render(request, 'valores.html', context)
#
#
# def ImunobioView(request):
#     context = {}
#     return render(request, 'imunobio/home.html', context)
#
#
# class AssistenteIndex(View):
#
#    def get(self, request, *args, **kwargs):
#       trabalhos = Trabalhos.objects.all()
#       trabalhos.id = Trabalhos.id
#       table = AssistenteTable(trabalhos)
#       table.paginate(page=request.GET.get("page", 1), per_page=5)
#       context = {"table": table, "id":id}
#       return render(request, 'assistente/index.html', context)
#
#    def post(self, request, *args, **kwargs):
#       trabalhos = Trabalhos.objects.all()
#       context = {"trabalhos": trabalhos}
#       return render(request, 'assistente/index.html', context)
#
#
# class AssistenteIndexDelete(View):
#
#     def get(self, request, *args, **kwargs):
#         # Excluir objeto
#         trabalho = Trabalhos.objects.get(id=kwargs.get("trabalho_id"))
#         # trabalho = Trabalhos.objects.get(id=kwargs.get("id"))
#         trabalho.delete()
#
#         # Excluir pasta
#         try:
#             shutil.rmtree(trabalho.directory)
#         except Exception as e:
#             logging.error(e)
#         trabalhos = Trabalhos.objects.all()
#         context = {"trabalhos": trabalhos}
#         return render(request, 'assistente/index.html', context)
#
#
# class AssistenteView(View):
#
#     def get(self, request, *args, **kwargs):
#         context = {}
#         return render(request, 'assistente/main.html', context)
#
#     def post(self, request, *args, **kwargs):
#         trabalho = Trabalhos()
#         description = request.POST.get('description', "no title")
#         trabalho.description = description
#         now = datetime.datetime.now()
#         trabalho.created_at = now
#         folder_name = "".join([x for x in f"{description}_{now}" if x.isdigit()])
#         user = request.user.username
#         trabalho.directory = os.path.join(WORKDIR, "media", user, folder_name)
#         try:
#             os.makedirs(trabalho.directory)
#         except Exception as e:
#             context = {"msg": f"Erro ao criar diretório. {e}"}
#             return render(request, 'assistente/main.html', context)
#         trabalho.save()
#         self.request.session["trabalho_id"] = trabalho.id
#         context = {}
#         return render(request, 'assistente/upload1.html', context)
#
#
# class AssistenteUploadView(View):
#
#     def post(self, request, *args, **kwargs):
#         if not len(request.FILES):
#             context = {"msg": "Nenhum arquivo selecionado"}
#             return render(request, 'assistente/upload1.html', context)
#         # Enviar o arquivo para a pasta correta
#         try:
#             trabalho_id = self.request.session["trabalho_id"]
#         except Exception as e:
#             logging.error(e)
#             trabalhos = Trabalhos.objects.all()
#             context = {"trabalhos": trabalhos}
#             return render(request, 'assistente/index.html', context)
#
#         trabalho = Trabalhos.objects.get(id=trabalho_id)
#         directory = trabalho.directory
#         filedata = request.FILES['file']
#         complete_file_path = f'{directory}/{filedata.name}'
#
#         logging.info("Tentando salvar arquivo")
#         try:
#             content = ContentFile(filedata.read())
#             if default_storage.exists(complete_file_path):
#                 default_storage.delete(complete_file_path)
#                 path = default_storage.save(complete_file_path, content)
#             else:
#                 path = default_storage.save(complete_file_path, content)
#         except Exception as e:
#             logging.error(f"DEFAULT_CHUNK_SIZE: {content.DEFAULT_CHUNK_SIZE}  -  {e}")
#             context = {"msg": str(e)}
#             return render(request, 'assistente/upload1.html', context)
#
#         filetype = magic.from_file(complete_file_path, mime=True)
#         accepted_types = ['application/csv', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet']
#         if filetype not in accepted_types:
#             try:
#                 os.remove(complete_file_path)
#             except Exception as e:
#                 logging.error(e)
#             context = {"msg": "Tipo de arquivo não suportado. Tente XLSX ou CSV"}
#             return render(request, 'assistente/upload1.html', context)
#
#         # Persistir os dados - serão salvos no schema do usuário
#         schema = request.user.username
#
#         analysis = Analysis()
#         try:
#             analysis.set_df_from_file(path, filetype)
#         except Exception as e:
#             logging.error(e)
#             context = {"msg": "Seu arquivo de input tem problemas. Verifique se todas as colunas "
#                               "possuem cabeçalho. Para uma lista completa de possíveis erros verifique"
#                               "a aba de ajuda."}
#             return render(request, 'assistente/upload1.html', context)
#
#         basename = filedata.name.split(".")[0]
#         rawtablename = f"rawtable_{trabalho_id}_{basename}"
#         varkeys, original_keys = analysis.persist(schema, rawtablename)
#
#         result = []
#         for index, key in enumerate(analysis.original_colnames):
#             varkey = varkeys[original_keys.index(key)]
#             if index == 0:
#                 result.append({"key": key, "varkey": varkey, "active": "active"})
#             else:
#
#                 result.append({"key": key, "varkey": varkey, "active": ""})
#
#         self.request.session["results"] = result
#         self.request.session["basename"] = basename
#         self.request.session["varkeys"] = varkeys
#         self.request.session["original_keys"] = original_keys
#         context = {"results": result, "basename": basename, "active_tab": result[0]["key"]}
#         return render(request, 'assistente/upload2.html', context)
#
#
# class ProcessDataView(View):
#
#     def get(self, request, *args, **kwargs):
#         context = {}
#         return render(request, 'assistente/processdata.html', context)
#
#     def post(self, request):
#         trabalho_id = self.request.session["trabalho_id"]
#         basename = self.request.session["basename"]
#         varkeys = self.request.session["varkeys"]
#         original_keys = self.request.session["original_keys"]
#         dict_varkeys = dict(zip(original_keys, varkeys))
#         schema = request.user.username
#         rawtablename = f"rawtable_{trabalho_id}_{basename}"
#
#         """Primeiro passo - deduplicar repetições com base nas chaves"""
#         analysis = Analysis()
#         analysis.set_df_from_db(schema, rawtablename)
#         present_keys = [x.replace("uniquekey_", "") for x in request.POST if "uniquekey_" in x]
#         # Criação da rawtable: deduplicada
#         analysis.dedup(rawtablename, schema, varkeys, present_keys)
#         types = {k.replace("datatype_", ""): v for k, v in request.POST.items() if "datatype_" in k}
#         adicional_args = {"estado": request.POST.get("estado")}
#         analysis.fixtypes(types, rawtablename + '_dedup', schema, **adicional_args)
#
#         df = analysis.df
#
#         result = self.request.session["results"]
#
#         context = {"results": result, "active_tab": result[0]["key"], "graph_div": {}, "trabalho_id": trabalho_id}
#
#         if type(df) == dict:  # foi erro
#             context["msg"] = str(df["msg"])
#             return render(request, 'assistente/upload2.html', context)
#
#         dt = df.dtypes
#         dc = list(df.columns)
#         for i in range(len(dt)):
#             colname = dc[i]
#             values = list(df[colname])
#             plot = PlotBio(values)
#             if "int" in dt[i].name or "float" in dt[i].name:
#                 graph = plot.dist_number()
#             else:
#                 graph = plot.dist_text()
#             try:
#                 result[i]["graph"] = graph
#             except IndexError:
#                 continue
#
#         return render(request, 'assistente/processdata.html', context)
#
#
# class TrabalhoView(View):
#
#     def get(self, request, *args, **kwargs):
#         trabalho_id = kwargs.get("trabalho_id")
#         trabalho = Trabalhos.objects.get(id=trabalho_id)
#         arquivos = Arquivos.objects.filter(trabalho_id=trabalho_id)
#         context = {"trabalho": trabalho,'arquivos':arquivos}
#         return render(request, 'assistente/trabalho/trabalho.html', context)
#
#     def post(self, request, *args, **kwargs):
#         trabalho_id = kwargs.get("trabalho_id")
#         trabalho = Trabalhos.objects.get(id=trabalho_id)
#         if kwargs.get("action") == 'delete':
#             trabalho.delete()
#             trabalhos = Trabalhos.objects.all()
#             context = {"trabalhos": trabalhos}
#             return render(request, 'assistente/index.html', context)
#
#         context = {"trabalho": trabalho}
#         return render(request, 'assistente/trabalho.html', context)
#
#
#
#
#
# class ArquivoView(View):
#
#     def post(self, request, *args, **kwargs):
#         caminho = settings.MEDIA_ROOT + f"{request.POST['trabalho_id']}/"
#         token = f"{secrets.token_hex(5)}_"
#         file = request.FILES['arquivo']
#         caminhoArquivo = default_storage.save(caminho+token+file.name, file)
#
#         arquivo = Arquivos(nome=request.POST['nome'],nomeArquivo=token+file.name, caminho=caminhoArquivo,
#                            trabalho_id=Trabalhos.objects.get(id=request.POST['trabalho_id']))
#         arquivo.save()
#
#         return redirect("trabalho", trabalho_id=request.POST['trabalho_id'])
