import django_tables2 as tables
from django.utils.safestring import mark_safe
from django_tables2.utils import A


class AssistenteTable(tables.Table):

    id = tables.columns.TemplateColumn(template_code=u"""{{ record.id }}""", orderable=True, verbose_name='id')

    name_project = tables.TemplateColumn('<a href="{% url "trabalho" record.id %}">{{record.description}}</a>',
                                         verbose_name='Nome do Projeto')

    # created_at = tables.columns.TemplateColumn(template_code=u"""{{ record.created_at }}""",
    #                                          orderable=True, verbose_name='Data')

    # deletar = tables.TemplateColumn('<a href="{% url "trabalho_delete" record.id %}">'
                                    # '<i class="fas fa-trash-alt"></i></a>')

    deletar = tables.LinkColumn('trabalho_delete', text="value",  args=[A('id')]) #id do trabalho

    class Meta:
        attrs = {'class': 'table table-bordered', 'id': 'dashboard_table'}
        template_name = "django_tables2/bootstrap4.html"

    @staticmethod
    def render_deletar(record, value):
        # d = {False: '-open', True: ''}
        return mark_safe(f'<i class="fas fa-trash-alt"></i>')
