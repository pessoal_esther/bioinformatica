import os

from django.db import models
from bioinformatica import settings

WORKDIR = settings.BASE_DIR

class Trabalhos(models.Model):
    id = models.AutoField(primary_key=True)
    description = models.TextField()
    rawtablename = models.TextField()
    directory = models.TextField(default=os.path.join(WORKDIR, "media", "temp"))
    created_at = models.DateTimeField()

class Arquivos(models.Model):
    id = models.AutoField(primary_key=True)
    nome = models.TextField()
    caminho = models.TextField()
    nomeArquivo = models.TextField()
    trabalho_id = models.ForeignKey(Trabalhos, on_delete=models.CASCADE)
