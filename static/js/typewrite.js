var typewriter = function(txt, classe){
  var i = 0;
  var speed = 50;

  function typeWriter() {
    if (i < txt.length) {
      document.getElementById(classe).innerHTML += txt.charAt(i);
      i++;
      setTimeout(typeWriter, speed);
    }
  }
}