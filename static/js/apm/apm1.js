const data = [40, 50, 25, 55, 70, 15]
const backgroundColor = []
for(i = 0; i < data.length; i++) {
    if(data[i] < 70) {
        backgroundColor.push('#819ACD')
    }
    if(data[i] >= 70) {
        backgroundColor.push('#2954AA')
    }
}
var ctx = document.getElementById('apm1').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['Barra 1', 'Barra 2', 'Barra 3', 'Barra 4', 'Barra 5', 'Barra 6'],
        datasets: [{
            label: 'Número de APM',
            data: data,
            backgroundColor: backgroundColor,
            borderColor: backgroundColor,
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    }
});