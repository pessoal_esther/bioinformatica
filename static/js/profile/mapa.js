function states(data_dict){
        // Prepare demo data
        // Data is joined to map using value of 'hc-key' property by default.
        // See API docs for 'joinBy' for more info on linking data and map.

       data_dict = data_dict.replaceAll("&quot;", '"')
       data_dict = JSON.parse(data_dict);

       var data = [];

       for (const [key, value] of Object.entries(data_dict)) {
          data.push([key,value]);
        }
        // criar o gráfico
        Highcharts.mapChart('container', {
          chart: {
            map: 'countries/br/br-all'
          },

          title: {
            text: ''
          },

          mapNavigation: {
            enabled: true,
            buttonOptions: {
              verticalAlign: 'bottom'
            }
          },

          colorAxis: {
            min: -0
          },

          series: [{
            data: data,
            name: 'Número de clientes',
            states: {
              hover: {
                color: '#3848d4'
              }
            },
            dataLabels: {
              enabled: true,
              format: '{point.name}'
            }
          }]
        });
}