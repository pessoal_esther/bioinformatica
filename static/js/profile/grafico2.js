
var ctx = document.getElementById("grafico2")

var chartGraph = new Chart(ctx, {
   type: 'pie',
   data: {
       labels: ["Feminino", "Masculino"],
       datasets: [{
           responsive: true,
           label: 'Gênero',
           backgroundColor: ['#819ACD', 'navy'],
           data: [559, 2433],
           borderWidth: 2
       }]
   }
})