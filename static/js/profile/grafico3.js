var ctx = document.getElementById("grafico3")

var chartGraph = new Chart(ctx, {
   type: 'bar',

   data: {
       labels: ["Motivo 1", "Motivo 2", "Motivo 3", "Motivo 4", "Motivo 5"],
       datasets: [{
           responsive: true,
           labels: 'Relação pessoas x motivo de empréstimo',
           backgroundColor: ['#819ACD', 'navy', '#2954AA', '#C1CEE5', '#98ACD6'],
           data: [340, 498, 700, 555, 82],
           borderWidth: 2
       }]
   },
   options: {
        plugins:{
            legend: {
                display: false
                 },
              }
         }
})