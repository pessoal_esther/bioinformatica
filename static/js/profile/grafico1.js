
var ctx = document.getElementById("grafico1")


var chart = new Chart(ctx, {
    type: 'pie',
    data: {
       labels: ["18-25", "26-35", "36-45", "46-60", "60+"],
       datasets: [{
        data: [6, 23, 41, 9, 21],
        backgroundColor: ['#819ACD', '#2954AA', '#C1CEE5', '#98ACD6', 'navy'],
       }]
    },
    options: {
       responsive: false,
       legend: {
          display: true,
          position: 'right',
          onClick: null
       },
    }
 });