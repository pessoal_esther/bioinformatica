var ctx = document.getElementById("grafico4")
var chartGraph = new Chart(ctx, {
   type: 'bar',
   data: {
       labels: ["< 1500", "1500 - 4000", "4000 8000", "8000 - 10000", " 10000 >"],
       datasets: [{
           responsive: true,
           backgroundColor: ['#819ACD', 'navy', '#2954AA', '#C1CEE5', '#98ACD6'],
           data: [23, 298, 340, 498, 700],
           borderWidth: 2
       }]
   },
   options: {
        plugins:{
            legend: {
                display: false
                 },
              }
         }
})



