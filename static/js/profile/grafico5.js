var ctx = document.getElementById("grafico5")
var chartGraph = new Chart(ctx, {
   type: 'bar',

   data: {
       labels: ["Jan", "Fev", "Mar", "Abril", "Maio", "Junho", "Julho",
        "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
       datasets: [{
           responsive: true,
           labels: 'Relação pessoas x motivo de empréstimo',
           backgroundColor: ['#819ACD', 'navy', '#2954AA', '#C1CEE5', '#98ACD6', '#819ACD', 'navy', '#2954AA', '#C1CEE5', '#98ACD6'],
           data: [78, 33, 101, 199, 171, 131, 111, 199, 98, 76, 54, 66],
           borderWidth: 2
       }]
   },
   options: {
        plugins:{
            legend: {
                display: false
                 },
              }
         }
})