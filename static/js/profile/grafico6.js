var ctx = document.getElementById("grafico6")
var chartGraph = new Chart(ctx, {
   type: 'bar',
   data: {
       labels: ['1 - 9', '10 - 29', '30 - 49', '50+'],
       datasets: [{
           responsive: true,
           backgroundColor: ['#819ACD', 'navy', '#2954AA', '#C1CEE5', '#98ACD6'],
           data: [4, 19, 22, 43, 61],
           borderWidth: 2
       }]
   },
   options: {

        indexAxis: 'y',
        plugins:{
            legend: {
                display: false
                 },
              }
         }
})