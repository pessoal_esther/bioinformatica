
function updateData(data){

d3.select("#chart").html("");

var xMin = d3.min(data, function (d) { return d.position; }),
    xMax = d3.max(data, function (d) { return d.position; });
var yMin = d3.min(data, function (d) { return d.coverage; }),
    yMax = d3.max(data, function (d) { return d.coverage; });

var margin = {top: 30, right: 20, bottom: 30, left: 50},
    width = 800 - margin.left - margin.right,
    height = 400 - margin.top - margin.bottom;

var plotChart = d3.select('#chart').classed('chart', true).append('svg')
    .attr('width', width + margin.left + margin.right)
    .attr('height', height + margin.top + margin.bottom )
    .append('g')
    .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

var plotArea = plotChart.append('g')
    .attr('clip-path', 'url(#plotAreaClip)');

plotArea.append('clipPath')
    .attr('id', 'plotAreaClip')
    .append('rect')
    .attr({ width: width, height: height});

var xScale = d3.scale.linear()
    .domain([xMin, xMax]).nice()
    .range([0, width]);
    yScale = d3.scale.linear()
    .domain([yMin, yMax]).nice()
    .range([height, 0]);







var line = d3.svg.line()
    .x(function(d) { return xScale(d.position); })
    .y(function(d) { return yScale(d.coverage); });

var xAxis = d3.svg.axis()
    .scale(xScale)
    .orient('bottom')
    .ticks(5),
    yAxis = d3.svg.axis()
    .scale(yScale)
    .orient('left');

plotChart.append('g')
    .attr('class', 'x axis')
    .attr('transform', 'translate(0,' + height + ')')
    .call(xAxis);

plotChart.append('g')
    .attr('class', 'y axis')
    .call(yAxis);


plotArea.append("path")
      .datum(data)
      .attr("class", "line")
      .attr("d", line);


var navWidth = width,
    navHeight = 100 - margin.top - margin.bottom;

var navChart = d3.select('#chart').classed('chart', true).append('svg')
    .classed('navigator', true)
    .attr('width', navWidth + margin.left + margin.right)
    .attr('height', navHeight + margin.top + margin.bottom)
    .append('g')
    .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

var navX = d3.scale.linear()
        .domain([xMin, xMax])
        .range([0, navWidth]),
    navY = d3.scale.linear()
        .domain([yMin, yMax])
        .range([navHeight, 0]);

var navXAxis = d3.svg.axis()
    .scale(navX)
    .orient('bottom');

navChart.append('g')
    .attr('class', 'x axis')
    .attr('transform', 'translate(0,' + navHeight + ')')
    .call(navXAxis);

var navData = d3.svg.area()
    .x(function (d) { return navX(d.position); })
    .y0(navHeight)
    .y1(function (d) { return navY(d.coverage); });

var navLine = d3.svg.line()
    .x(function (d) { return navX(d.position); })
    .y(function (d) { return navY(d.coverage); });

navChart.append('path')
    .attr('class', 'data')
    .attr('d', navData(data));

navChart.append('path')
    .attr('class', 'line')
    .attr('d', navLine(data));


var viewport = d3.svg.brush()
    .x(navX)
    .on("brush", function () {
        xScale.domain(viewport.empty() ? navX.domain() : viewport.extent());
        redrawChart();
    });

navChart.append("g")
    .attr("class", "viewport")
    .call(viewport)
    .selectAll("rect")
    .attr("height", navHeight);


var zoom = d3.behavior.zoom()
    .x(xScale)
    .on('zoom', function() {
        if (xScale.domain()[0] < xMin) {
	    var x = zoom.translate()[0] - xScale(xMin) + xScale.range()[0];
            zoom.translate([x, 0]);
        } else if (xScale.domain()[1] > xMax) {
	    var x = zoom.translate()[0] - xScale(xMax) + xScale.range()[1];
            zoom.translate([x, 0]);
        }
        redrawChart();
        updateViewportFromChart();
    });


var overlay = d3.svg.area()
    .x(function (d) { return xScale(d.position); })
    .y0(0)
    .y1(height);

plotArea.append('path')
    .attr('class', 'overlay')
    .attr('d', overlay(data))
    .call(zoom);

viewport.on("brushend", function () {
       //updateZoomFromChart();
    });

//the circle should be the last element added, so we can add more events on it.
var dots = plotArea.selectAll('.dots')
    .data(data)
    .enter()
    .append('circle')
    .attr('class','dots')
    .attr('cx',function(d){return xScale(d.position);})
    .attr('cy',function(d){return yScale(d.coverage);})
    .attr('r',3)
    .on("mouseover",function(d){d3.select(this).attr('r',6);})
    .on("mouseleave",function(d){d3.select(this).attr('r',3);})
    .on("click",function(){alert("Should I provide more information, or show the circles in different colors?")});


d3.selectAll(".dots").append('title').text(function(d){return ('position: ' + d.position +
							     '\ncov_forward: ' + d.forward +
							     '\ncov_reverse: ' + d.reverse +
							     '\ncov_total: '   + d.coverage)});




function updateZoomFromChart() {

    zoom.x(xScale);

    var fullDomain = xMax - xMin,
        currentDomain = xScale.domain()[1] - xScale.domain()[0];

    var minScale = currentDomain / fullDomain,
        maxScale = minScale * 20;

    zoom.scaleExtent([minScale, maxScale]);
}





function redrawChart() {

   //essa parte esta ruim, melhor usar componentes. Mas ainda nao sei fazer.
   //preciso aprender a programar em javascript.

    plotChart.selectAll('.dots')
    .attr('cx',function(d){return xScale(d.position);})
    .attr('cy',function(d){return yScale(d.coverage);});
    plotArea.selectAll('.line').attr("d", line);
    plotChart.select('.x.axis').call(xAxis);
}


function updateViewportFromChart() {

    if ((xScale.domain()[0] <= xMin) && (xScale.domain()[1] >= xMax)) {

        viewport.clear();
    }
    else {

        viewport.extent(xScale.domain());
    }

    navChart.select('.viewport').call(viewport);
}




xScale.domain([xMax-200,xMax]);

redrawChart();
updateViewportFromChart();
updateZoomFromChart();





}
// Main function

d3.tsv('Resistance.txt', function(error, lines) {

// creates one set of data for each amplicon
  var amplicons = {}

  lines.forEach(function(amp) {
    var ampName = amp.amplicon;
    if ( amplicons[ampName] === undefined ) {
      amplicons[ampName] = [];
    }
    amp.position = +amp.position;
    amp.coverage = +amp.cov;
    amplicons[ampName].push(amp)
  });


  var selectmenu = d3.select('#listaAmplicons').append("select");
// create menu
  for (var amp in amplicons){
   selectmenu
     .append("option")
     .text(amp)
     .attr("class","menus");
  }

selectmenu.on("change", function(d){
  updateData(amplicons[(this[this.selectedIndex]).text])
});


// start first graph
  for (first in amplicons) break;
  data = amplicons[first];
  updateData(data)

});







