 $(document).ready(function() {
			 d3.selection.prototype.moveToFront = function() { 
				return this.each(function() { 
					this.parentNode.appendChild(this); 
					}); 
				}; 
	 
             var margin = {
                     top: 20,
                     right: 50,
                     bottom: 100,
                     left: 115
                 },
                 width = 960 - margin.left - margin.right,
                 height = 500 - margin.top - margin.bottom;


             var wordsTable = [];

             var bigData;
             var questions = [];

             d3.csv("csv/questions.csv", function(error, data) {
                     if (error) throw error;

                     data.forEach(function(d) {
                         questions[d.id] = d;
                     })

                     d3.csv("csv/results.csv", function(error, data) {
                             if (error) throw error;

                             bigData = data;

                             questionsDetail = [];
                             for (var i = 1; i < 53; i++) {

                                 questionsDetail[i] = [];
                             }
                             votes = [];
                             votes['mean'] = [];
                             data.forEach(function(d) {
                                 for (var i = 1; i < 53; i++) {
                                     d[i] = +d[i];
                                     questionsDetail[i].push(d[i])
                                 };
                                 if (!votes[d.branch]) {
                                     votes[d.branch] = [];

                                 }
                                 votes[d.branch].push(d)
                                 if (!votes[d.senioridade]) {

                                     votes[d.senioridade] = [];
                                 }
                                 votes[d.senioridade].push(d)

                                 votes['mean'].push(d)

                             });


                             mean = []
                             for (d in votes) {
                                 mean[d] = {};
                                 for (e in votes[d]) {

                                     for (i in votes[d][e]) {
                                         if (!mean[d][i]) {
                                             mean[d][i] = 0;
                                         }
                                         mean[d][i] += Math.ceil((votes[d][e][i] / votes[d].length) * 100) / 100;

                                     }

                                 }

                             }

                             dataArea = [];

                             var surveyArea = ['geral', 'performance', 'leadership', 'manager', 'people', 'you', 'culture', 'crescimento'];

                             var areaName = {
                                 'geral': 'Geral',
                                 'performance': 'Performance',
                                 'leadership': 'Leadership',
                                 'manager': 'My Manager',
                                 'people': 'People and Teamwork',
                                 'you': 'You',
                                 'culture': 'Culture',
                                 'crescimento': 'Crescimento & Desenvolvimento'
                             };

                             var color = d3.scale.category20();

                             d3.selectAll(".cbLabe").forEach(function(d, i) {
                                 d.forEach(function(d, i) {
                                     d3.select(d).style('color', color(i))
                                 })
                             })
                             var area = 'geral',
                                 branch = "Sales",
                                 senioriade = "Analista";
                             draw(area, branch, senioriade);

                             function draw(questionArea, branch, role) {

                                 var ticks = [];
                                 for (var i = 0; i < surveyArea.length; i++) {
                                     dataArea[surveyArea[i]] = [];
                                     ticks[surveyArea[i]] = [];
                                     for (var j = 1; j < questions.length; j++) {


                                         if (questions[j].area == areaName[surveyArea[i]]) {

                                             ticks[surveyArea[i]].push(questions[j].question);
                                         }


                                     };
                                 };


                                 var firstQuestion = 0;
                                 var lastQuestion = 0;
                                 var questionNo = 0;
                                 var dataGroup = [];
                                 var svg =   [],
                                     xScale = [],
                                     yScale = [],
                                     xAxis = [],
                                     yAxis = [];
                                 var lineGen = [];


                                 var tip = d3.tip()
                                     .attr('class', 'd3-tip')
                                     .offset([-10, 0])
                                     .html(function(d, i) {

                                         return '<span style="color: red;"> ' + d['1'] + '</span> people voted 1.</br>'+
                                                '<span style="color: red;"> ' + d['2'] + '</span> people voted 2.</br>'+
                                                '<span style="color: red;"> ' + d['3'] + '</span> people voted 3.</br>'+
                                                '<span style="color: red;"> ' + d['4'] + '</span> people voted 4.</br>'+
                                                '<span style="color: red;"> ' + d['5'] + '</span> people voted 5.';
                                     })

                                 questions.forEach(function(d) {
                                     if (d.area == areaName[questionArea]) {

                                         for (a in mean) {

                                             dataArea[questionArea].push({
                                                 type: a,
                                                 question: d.id,
                                                 vote: mean[a][d.id]
                                             });
                                         }

                                     }

                                 })

								 
                                 document.getElementById("graphArea").innerHTML = "";
                                 svg[questionArea] = d3.select('#graphArea').append("svg")
                                     .attr("width", width + margin.left + margin.right)
                                     .attr("height", height + margin.top + margin.bottom)
                                     .append("g")
                                     .attr("transform", "translate(" + margin.left + "," + margin.top + ")").call(tip);

								
										   
									
                                 firstQuestion = lastQuestion + 1;


                                 lastQuestion += ticks[questionArea].length;

                                 xScale[questionArea] = d3.scale.linear()
                                     .range([0, width])
                                     .domain([firstQuestion - .2, lastQuestion]);


                                 yScale[questionArea] = d3.scale.linear()
                                     .range([height, 0])
                                     .domain([0, 5]),
                                     xAxis[questionArea] = d3.svg.axis()
                                     .scale(xScale[questionArea])

                                 .ticks(ticks[questionArea].length - 1)
                                     .tickFormat(function(d) {

                                         return ticks[questionArea][d - firstQuestion]
                                     }),

                                     yAxis[questionArea] = d3.svg.axis()

                                 .scale(yScale[questionArea])
                                     .orient("left")
									 .innerTickSize(-width);


                                 svg[questionArea].append("svg:g")
                                     .attr("class", "x axis ")
                                     .attr("transform", "translate(0," + height + ")")
                                     .call(xAxis[questionArea])
                                     .selectAll(".tick text")
                                     .call(wrap, width / ticks[questionArea].length, i);

                                 svg[questionArea].append("svg:g")
                                     .attr("class", "y axis ")

                                 .call(yAxis[questionArea])
                                     .append("text")
                                     .attr("transform", "rotate(-90)")
                                     .attr("y", 6)
                                     .attr("dy", ".71em")
                                     .style("text-anchor", "end")
                                     .text("Nota");

                                 var data = getData(branch, role);

								var leg = svg[questionArea].append("g");
								leg.append("rect").attr("class","recLeg")
								           .attr("x","500")
										   .attr("y","200")										  
										   .attr("width","290")
										   .attr("height","60")
										   .attr("rx","2")
										   .attr("ry","2");
								leg.append("text").attr("x","550").attr("y","220")
									.text("Média da amostra selecionada");
								leg.append("text").attr("x","550").attr("y","250")
									.text("Média de todos os funcionários");

								leg.append("circle").attr("cx","530").attr("cy","215")
									.attr("r","10").attr("class","c2");
								leg.append("circle").attr("cx","530").attr("cy","245")
									.attr("r","10");
                                 data.forEach(function(d, i) {


                                     if (d.question <= lastQuestion) {




                                         svg[questionArea].datum(d).append('circle')
                                             .attr("cx", xScale[questionArea](d.question))
                                             .attr("cy", yScale[questionArea](d.vote))
                                             .attr("r", function() {
                                                 if (d.color == 'black') {
                                                     return 8;

                                                 } else {
                                                     return 10;
                                                 }
                                             })
                                             .style("fill", d.color)
                                             .attr('stroke-width', 2)
                                             .style("stroke", d.color)
                                             .on('mouseover', function() {

                                                 if (d.color != "black") {

                                                     tip.show(d);
                                                 }
                                             })
                                             .on('mouseout', tip.hide)


                                     }
									
									
                                 });
									
                                 function getData(a, b) {
                                     var result = [];
                                     var countMean = [];
                                     bigData.forEach(function(d, i) {
                                         if (d.branch == a && d.senioridade == b) {

                                             for (var i = 1; i < 55; i++) {
                                                 result.push({
                                                     'question': +i,
                                                     'vote': d[i]


                                                 });



                                             }
                                         }


                                     });
                                     var finalResult = [];
                                     for (var i = 1; i < 53; i++) {

                                         finalResult[i] = {
                                             "question": i,
                                             "vote": 0,
                                             'color': 'lightgreen',
                                             '1': 0,
                                             '2': 0,
                                             '3': 0,
                                             '4': 0,
                                             '5': 0,
                                         }
                                         result.forEach(function(d) {
                                             if (d.question == i) {
                                                 finalResult[i].vote += d.vote;
                                                 finalResult[i][d.vote] += 1;

                                             }
                                         })

                                     }
                                         finalResult.forEach(function(d) {
                                             d.vote = d.vote / (result.length / 53);
                                         });
                                         for (i = 53; i < 55; i++) {
                                             wordsTable[i - 53] = '';
                                             result.forEach(function(d) {
                                                 if (d.question == i) {
                                                     wordsTable[i - 53] += " " + d.vote;

                                                 }
                                             });
                                             wordsTable[i - 53] = wordsTable[i - 53].toLowerCase();

                                         }

                                         if (isNaN(finalResult[10].vote)) {
                                             return 0;
                                         }
                                         for (a in mean.mean) {

                                             finalResult.push({
                                                 'question': a,
                                                 'vote': +mean.mean[a],
                                                 'color': 'black'
                                             });

                                         };
                                         wordsTable[0] = getFrequency2(wordsTable[0]);
                                         wordsTable[1] = getFrequency2(wordsTable[1]);
                                         console.log(wordsTable);
                                         return finalResult;
                                     }

                                     function wrap(text, width, no) {

                                         text.each(function(d, i) {
                                             questionNo += 1;
                                             var text = d3.select(this),
                                                 words = text.text().split(/\s+/).reverse(),
                                                 word,
                                                 line = [],
                                                 lineNumber = 0,
                                                 lineHeight = 1.1, // ems
                                                 y = text.attr("y"),
                                                 dy = parseFloat(text.attr("dy")),
                                                 tspan = text.text(null).append("tspan").attr("class", "qu" + questionNo).attr("x", 0).attr("y", y).attr("dy", dy + "em");
                                             while (word = words.pop()) {
                                                 line.push(word);
                                                 tspan.text(line.join(" "));
                                                 if (tspan.node().getComputedTextLength() > width) {
                                                     line.pop();
                                                     tspan.text(line.join(" "));
                                                     line = [word];
                                                     tspan = text.append("tspan").attr("x", 0).attr("class", 'qu' + questionNo).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em").text(word);
                                                 }
                                             }
                                         });
                                     }
                                     d3.select("#area").on("change", function(d) {

                                         area = d3.select("#area").property("value");
                                         draw(area, branch, senioriade);

                                     })
                                     d3.select("#branch").on("change", function(d) {

                                         branch = d3.select("#branch").property("value")
                                         draw(area, branch, senioriade);
                                     })
                                     d3.select("#senioriade").on("change", function(d) {

                                         senioriade = d3.select("#senioriade").property("value")
                                         draw(area, branch, senioriade);
                                     })




                                     var fill = d3.scale.category20();

                                     function getFrequency2(string, cutOff) {
                                         var cleanString = string.replace(/[\.,-\/#!$%\^&\*;:{}=\-_`~()]/g, ""),
                                             words = cleanString.split(' '),
                                             frequencies = {},
                                             word, frequency, i;

                                         for (i = 0; i < words.length; i++) {
                                             word = words[i];
                                             frequencies[word] = frequencies[word] || 0;
                                             frequencies[word]++;
                                         }

                                         words = Object.keys(frequencies);
                                         var result = [];
                                         words.forEach(function(d) {
                                             result.push({
                                                 'text': d,
                                                 'size': frequencies[d]
                                             });
                                         });


                                             console.log(result);
                                             return result;


                                     }
                                     console.log(wordsTable[0]);
                                     
									 
									 
                                     var layout1 = d3.layout.cloud()
                                         .size([440, 300])
                                         .words(wordsTable[0].map(function(d) {
                                             return {
                                                 text: d.text,
                                                 size:  d.size*d.size* 15 + 5
                                             };
                                         }))
                                         .padding(5)
                                         .rotate(function() {
                                             return (~~(Math.random() * 6) - 3) * 30;
                                         })
                                         .spiral('archimedean')
                                         .font("Impact")
                                         .fontSize(function(d) {
                                             return d.size;
                                         })
                                         .on("end", drawWord1);
                                        var layout2 = d3.layout.cloud()
                                         .size([440, 300])
                                         .words(wordsTable[1].map(function(d) {
                                             return {
                                                 text: d.text,
                                                 size:  d.size*d.size* 15 + 5
                                             };
                                         }))
                                         .padding(5)
                                         .rotate(function() {
                                             return (~~(Math.random() * 6) - 3) * 30;
                                         })
                                         .spiral('archimedean')
                                         .font("Impact")
                                         .fontSize(function(d) {
                                             return d.size;
                                         })
                                         .on("end", drawWord2); 

                                     layout1.start();
                                     layout2.start();

                                     function drawWord1(words) {
                                         document.getElementById("wc1").innerHTML = "";
                                         d3.select("#wc1").append("svg")
                                             .attr("width", layout1.size()[0])
                                             .attr("height", layout1.size()[1])
                                             .append("g")
                                             .attr("transform", "translate(" + layout1.size()[0] / 2 + "," + layout1.size()[1] / 2 + ")")
                                             .selectAll("text")
                                             .data(words)
                                             .enter().append("text")
                                             .style("font-size", function(d) {
                                                 return d.size + "px";
                                             })
                                             .style("font-family", "Impact")
                                             .style("fill", function(d, i) {
                                                 return fill(i);
                                             })
                                             .attr("text-anchor", "middle")
                                             .attr("transform", function(d) {
                                                 return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
                                             })
                                             .text(function(d) {
                                                 return d.text;
                                             });
                                     }
                                     function drawWord2(words) {
                                         document.getElementById("wc2").innerHTML = "";
                                         d3.select("#wc2").append("svg")
                                             .attr("width", layout2.size()[0])
                                             .attr("height", layout2.size()[1])
                                             .append("g")
                                             .attr("transform", "translate(" + layout2.size()[0] / 2 + "," + layout2.size()[1] / 2 + ")")
                                             .selectAll("text")
                                             .data(words)
                                             .enter().append("text")
                                             .style("font-size", function(d) {
                                                 return d.size + "px";
                                             })
                                             .style("font-family", "Impact")
                                             .style("fill", function(d, i) {
                                                 return fill(i);
                                             })
                                             .attr("text-anchor", "middle")
                                             .attr("transform", function(d) {
                                                 return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
                                             })
                                             .text(function(d) {
                                                 return d.text;
                                             });
                                     }
                                 }
                             });
                     });
					 
             });