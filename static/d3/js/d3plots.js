var margins = {"left":30, "right":30, "top":30, "bottom":30};
var width = 400;
var height = 400;

var svg = d3.select("#plot2").append("svg").attr("width", width).attr("height", height).append("g")
.attr("transform", "translate(" + margins.left + "," + margins.top + ")");

svg.append("g").attr("class", "x axis").attr("transform", "translate(0,300)");
svg.append("g").attr("class", "y axis");

var x = d3.scale.linear().domain([0,13]).range([0,300]);
var y = d3.scale.linear().domain([0,13]).range([150,0]);

function update(limX,limY,data,cor){

	x = d3.scale.linear().domain(limX).range([0,400]);
	y = d3.scale.linear().domain(limY).range([300,0]);
    var xAxis = d3.svg.axis().scale(x).orient("bottom");
    var yAxis = d3.svg.axis().scale(y).orient("left");
    
	svg.selectAll("g.y.axis").transition().call(yAxis);
	svg.selectAll("g.x.axis").transition().call(xAxis);   
    
	var mydata = svg.selectAll(".node").data(data);
	
	mydata.enter().append("circle")	
		.attr("class","node")
		
	mydata.transition().duration(500)
		.attr("cx",function(d){return x(d.x);})
		.attr("cy",function(d){return y(d.y);})
		.attr("r",5).style("fill",cor);    

	mydata.exit().remove();
	}

var data1 = [{x:10,y:12},{x:5,y:1},{x:7,y:3},{x:8,y:6},{x:1,y:8},{x:3,y:11}];
var data2 = [{x:15,y:2},{x:15,y:11},{x:0.7,y:13},{x:.8,y:16},{x:11,y:.8}];

update([0,13],[0,13],data1);

d3.select("#bt1").on("click",function(){return update([0,13],[0,13],data1,"green")});
d3.select("#bt2").on("click",function(){return update([0,17],[0,17],data2,"red")});
