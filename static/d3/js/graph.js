
var margin = {top: 30, right: 50, bottom: 100, left: 100};
var  width = 1000 - margin.left - margin.right;
var height = 600 - margin.top - margin.bottom;
  

var min = 0,
    max = 10;


d3.csv("/data/data.csv", function(error, lines) {
  
  var data = [];
  var pontos;
  
  var employees = {};
  
  lines.forEach(function(x) {
    if (employees[x[""]]===undefined){
      employees[x[""]]=[+x.Geral];
    }
    else{
      employees[x[""]].push(+x.Geral);
    }
  });

    for (var key in employees) {
      if (employees.hasOwnProperty(key)) {
        var d0 = key;
        var d1 = employees[key];
        var d = [d0,d1];
        data.push(d);
      }
    }
  
  
  var chart = d3.box()
    .whiskers(iqr(1.5))
    .height(height) 
    .domain([min, max]);
    

  var svg = d3.select("#graph").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .attr("class", "box")    
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
  
  // the x-axis
  var x = d3.scale.ordinal()     
    .domain( data.map(function(d) { return d[0] } ) )     
    .rangeRoundBands([0 , width], 0.7, 0.3);    

  var xAxis = d3.svg.axis()
    .scale(x)
    .orient("bottom");

  // the y-axis
  var y = d3.scale.linear()
    .domain([min, max])
    .range([height + margin.top, 0 + margin.top]);
  
  var yAxis = d3.svg.axis()
    .scale(y)
    .orient("left");

  // draw the boxplots  
  svg.selectAll(".box")    
  .data(data)
  .enter().append("g")
  .attr("transform", function(d) { return "translate(" +  x(d[0])  + "," + margin.top + ")"; } )
      .call(chart.width(x.rangeBand())); 
  
        
  
 
   // draw y axis
  svg.append("g")
        .attr("class", "y axis")
        .call(yAxis)
    .append("text") // and text1
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", ".71em")
      .style("text-anchor", "end")
      .style("font-size", "16px") 
      .text("Satisfaction");    
  
  // draw x axis  
  svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + (height  + margin.top + 10) + ")")
      .call(xAxis)
    .append("text")             // text label for the x axis
        .attr("x", (width) )
        .attr("y",  25 )
    .attr("dy", ".71em")
        .style("text-anchor", "middle")
    .style("font-size", "16px") 
        .text(""); 
});




// Returns a function to compute the interquartile range.
function iqr(k) {
  return function(d, i) {
    var q1 = d.quartiles[0],
        q3 = d.quartiles[2],
        iqr = (q3 - q1) * k,
        i = -1,
        j = d.length;
    while (d[++i] < q1 - iqr);
    while (d[--j] > q3 + iqr);
    return [i, j];
  };
}
