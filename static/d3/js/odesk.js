function graph1(){

function updateData(data){

//clear data
//d3.select("#chart").html("");
  
var margin = {top: 30, right: 20, bottom: 30, left: 50},
    width = 800 - margin.left - margin.right,
    height = 400 - margin.top - margin.bottom;

var plotChart = d3.select('#chart').append('svg')
    .attr('width', width + margin.left + margin.right)
    .attr('height', height + margin.top + margin.bottom )
    .append('g')
    .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

var line = d3.svg.line()
    .x(function(d) { return xScale(d.position); })
    .y(function(d) { return yScale(d.coverage); });    
    

var cluster = d3.layout.cluster();

var root = cluster(data);
var nodes = cluster.nodes(data);
var links = cluster.links(nodes.links);


var dots = plotChart.selectAll(".dots").data(nodes).enter().append("g");

dots.append("circle")		     
		     .attr("cx",function(d){return d.x;})
		     .attr("cy",function(d){return d.y;})
		     .attr("r",5);
dots.append("text").text(function(d){return d.name})
      .attr("x",function(d){return d.x - 10})
      .attr("y",function(d){return d.y - 10});
// 
var para = "stop";
    
}    
// Main function 

d3.json('/data/odesk.json', function(error, root) {
  
updateData(root)
  
});


}


