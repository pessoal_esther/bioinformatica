function graph2(){

var margin = {top: 30, right: 50, bottom: 100, left: 100};
var  width = 1000 - margin.left - margin.right;
var height = 600 - margin.top - margin.bottom;
	

var min = Infinity,
    max = -Infinity;

    
function isData(d,a){
  var b = true;
  d.forEach(function(x){var c = x[0]; 
			var f = a[0];
			if (x[0]==a[0]){b=false}});
  return b;
}
    

d3.tsv("data/Resistance.txt", function(error, lines) {
	
	var data = [];
	var covs;
	
	lines.forEach(function(x) {
	  var amp = [x.amplicon],
	  pos = +x.position-1,
	  cov = +x.cov;
	  
	  //zera covs e adiciona a primeira
	  
	  var test = isData(data,amp);
	  if (isData(data,amp)) {
	    data.push(amp); 
	    covs = [];
	    covs.push(cov);
	    amp.push(covs);}
	    
	  //se nao incrementa covs
	  
	 else{covs.push(cov);  }
	 
	  
	  if (cov > max) max = cov;
	  if (cov < min) min = cov;
		    
	});
	
	
	var chart = d3.box()
		.whiskers(iqr(1.5))
		.height(height)	
		.domain([min, max]);
		

	var svg = d3.select("#viz2").append("svg")
		.attr("width", width + margin.left + margin.right)
		.attr("height", height + margin.top + margin.bottom)
		.attr("class", "box")    
		.append("g")
		.attr("transform", "translate(" + margin.left + "," + margin.top + ")");
	
	// the x-axis
	var x = d3.scale.ordinal()	   
		.domain( data.map(function(d) { return d[0] } ) )	    
		.rangeRoundBands([0 , width], 0.7, 0.3); 		

	var xAxis = d3.svg.axis()
		.scale(x)
		.orient("bottom");

	// the y-axis
	var y = d3.scale.linear()
		.domain([min, max])
		.range([height + margin.top, 0 + margin.top]);
	
	var yAxis = d3.svg.axis()
    .scale(y)
    .orient("left");

	// draw the boxplots	
	svg.selectAll(".box")	   
	.data(data)
	.enter().append("g")
	.attr("transform", function(d) { return "translate(" +  x(d[0])  + "," + margin.top + ")"; } )
      .call(chart.width(x.rangeBand())); 
	
	      
	
 
	 // draw y axis
	svg.append("g")
        .attr("class", "y axis")
        .call(yAxis)
		.append("text") // and text1
		  .attr("transform", "rotate(-90)")
		  .attr("y", 6)
		  .attr("dy", ".71em")
		  .style("text-anchor", "end")
		  .style("font-size", "16px") 
		  .text("Coverage");		
	
	// draw x axis	
	svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + (height  + margin.top + 10) + ")")
      .call(xAxis)
	  .append("text")             // text label for the x axis
        .attr("x", (width) )
        .attr("y",  25 )
		.attr("dy", ".71em")
        .style("text-anchor", "middle")
		.style("font-size", "16px") 
        .text("Amplicons"); 
});




// Returns a function to compute the interquartile range.
function iqr(k) {
  return function(d, i) {
    var q1 = d.quartiles[0],
        q3 = d.quartiles[2],
        iqr = (q3 - q1) * k,
        i = -1,
        j = d.length;
    while (d[++i] < q1 - iqr);
    while (d[--j] > q3 + iqr);
    return [i, j];
  };
}


}















