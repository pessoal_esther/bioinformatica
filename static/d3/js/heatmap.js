var NOQ = 52; //number of questions
var colors = ['#ff0000', '#ff6101', '#ff8c17', '#ffac31', '#ffc54d', '#ffda6b', '#ffea8c', '#fff5b0', '#fffcd6', '#ffffff'];
var minScore = 5; //starts with 5
var maxScore = 5;
var	cell = 70;
var	margin = {top: 40, right: 60, bottom: 30, left: 50};
var	width = 1000 - margin.left - margin.right;
var	height = 700 - margin.top - margin.bottom;
var	svg1 = d3.select("#chart1")
	        .append("svg").attr("class","svg_heatmap")
	        .attr("width", width + margin.left + margin.right)
	        .attr("height", height + margin.top + margin.bottom)
	        .append("g")
	        .attr("transform","translate(" + margin.left + "," + margin.top + ")");

var	svg2 = d3.select("#chart2")
	        .append("svg").attr("class","svg_heatmap")
	        .attr("width", width + margin.left + margin.right)
	        .attr("height", height + margin.top + margin.bottom)
	        .append("g")
	        .attr("transform","translate(" + margin.left + "," + margin.top + ")");
			
var questionNumbers = [];
var meanByBranch;
var meanBySenior;



for (var x=1;x<=NOQ;x++){
	questionNumbers.push(x.toString());
}

function obj2array(m){
	var output = {x:[],y:[],a:[]};//0:xvalues;1:yvalues;2:array
	for (var r in m){
		if (m.hasOwnProperty(r)) {
			output.y.push(r);
			var cArray = [];
			for (var c in m[r]){
				if (m[r].hasOwnProperty(c)) {
					if (!(output.x.indexOf(c) in output.x)){
						output.x.push(c);
					}
					cArray.push(m[r][c]);
					if (m[r][c]<minScore){
						minScore = m[r][c]; //find min score
					}
				}
			}	
		output.a.push(cArray);
		}
	}
return output;
}
			
function drawMatrix(svg,m){
	
	//convert matrix to array
	amatrix = obj2array(m)
	textY = amatrix.y;
	textX = amatrix.x;
	matrix = amatrix.a;
	
	//draw boxes
	for (var i=0;i<matrix.length;i++){
		svg.selectAll('.boxes'+i.toString()).data(matrix[i]).enter().append("rect")
	   .attr("class","boxes boxes"+i.toString())
	   .attr("x",function(d,ix){return ix*cell;})
	   .attr("y",margin.top+i*cell)
	   .attr("rx",5)
	   .attr("ry",5)
	   .attr("width",cell)
	   .attr("height",cell)
	   .style("fill",function(d){return color(d)})
	   .append("svg:title").text(function(d){return d.toFixed(2);});
	}
	
	//draw X-labels
	var svgX = svg.append("g").classed("svgX",true);
	svgX.selectAll(".svgXText").data(textX).enter().append("text")
		.attr("x",function(d,i){return margin.left+i*cell-cell/2})
		.attr("y",margin.top)
		.text(function(d){return d})
		.attr("transform",function(d,i){
				var x = margin.left+i*cell-cell/2; 
				var y = margin.top;
				return "rotate(320, "+x+" , "+y+")"
				});
		
	//draw Y-labels
	var svgY = svg.append("g").classed("svgY",true);
	svgY.selectAll(".svgYText").data(textY).enter().append("text")
		.attr("x",10+textX.length*cell)
		.attr("y",function(d,i){return margin.top + i*cell + cell/2})
		.text(function(d){return d});	

	//draw Scale
	var color_scale = svg.append("g").classed('color_scale',true);
	color_scale.selectAll('color_scale_rect').data(colors).enter().append("rect")
						  .attr("class",".color_scale_rect")
						  .attr("x",function(d,i){return i*cell/3})
						  .attr("y",margin.top + textY.length*cell+cell)
						  .attr("width",cell/3)
						  .attr("height",cell/3)
						  .style("fill",function(d,i){return d})
						  .style("stroke","black");
	color_scale.append("text")
	           .attr("x","0")
	           .attr("y",textY.length*cell+3*cell/2)
			   .text(minScore.toFixed(2))
			   .append("svg:title").text("Minimun score observed");						  
	color_scale.append("text")
	           .attr("x",10*cell/3-cell/6)
	           .attr("y",textY.length*cell+3*cell/2)
			   .text("5");
			   					  

	
	}

function color(d){
	var index = parseInt((10*d-10*minScore)/(5-minScore))
	return colors[index];
}

function mean(lists){
	//[[1,2,3],[5,6,7]] = [3,4,5]
	var l = [];
	for (var i=0;i<lists[0].length;i++){
		var sum = 0;
		for (var j=0;j<lists.length;j++){
			sum = sum + lists[j][i];
		}
		l.push(sum/lists.length);
	}
	return l;
}

function meanObj(obj){

	return newObj;
}

function getMean(column,lines){
	var a = {};
	lines.forEach(function(d){
		var scores = [];
		questionNumbers.forEach(function(e){scores.push(+d[e])});
		if (a[d[column]]===undefined){
			a[d[column]] = [scores];
		}
		else{
			a[d[column]].push(scores);
		}
	});
	
	(d3.keys(a)).forEach(function(d){
		a[d]=mean(a[d]);
	});
	return a;
}

function meanArray(array){
	var sum = _.reduce(array,function(t,n){
		return t+n;
	});
	return sum/array.length;
}	

//Main function
d3.csv('/csv/results.csv', function(error, lineResults) {
	
	meanByBranch = getMean('branch',lineResults);
	meanBySenior = getMean('senioridade',lineResults);

	d3.csv('/csv/questions.csv', function(error, lineQuestions) {
		questionByArea = {};
		lineQuestions.forEach(function(d){
			if (questionByArea[d.area]===undefined){
				questionByArea[d.area]=[+d.id-1];
			}
			else{
				questionByArea[d.area].push(+d.id-1);
			}
		});
		// skip "a melhorar" - BY BRANCH
		var matrix={};
		for (var branch in meanByBranch){
			if (meanByBranch.hasOwnProperty(branch)) {
				matrix[branch]=[];
				for (var q in questionByArea){
					if (questionByArea.hasOwnProperty(q) 
						&& q!=="A Melhorar"  && q!=="Excelente") {
						var lq = [];
						nq = questionByArea[q];
						nq.forEach(function(d){
							lq.push(meanByBranch[branch][d]);
						});
						matrix[branch][q]=meanArray(lq);
					}
				}
   			}
		}
		drawMatrix(svg1,matrix);
		
		//BY SENIORITY
		var matrix={};
		for (var senior in meanBySenior){
			if (meanBySenior.hasOwnProperty(senior)) {
				matrix[senior]=[];
				for (var q in questionByArea){
					if (questionByArea.hasOwnProperty(q) 
						&& q!=="A Melhorar"  && q!=="Excelente") {
						var lq = [];
						nq = questionByArea[q];
						nq.forEach(function(d){
							lq.push(meanBySenior[senior][d]);
						});
						matrix[senior][q]=meanArray(lq);
					}
				}
   			}
		}
		drawMatrix(svg2,matrix);
		
		
	});
		
});		
  

  
  
  