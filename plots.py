import plotly.graph_objects as go
import plotly as plotly

class PlotBio:
    def __init__(self, data):
        self.data = data

    def dist_number(self):
        fig = go.Figure(
            data=[go.Bar(y=self.data)],
            layout_title_text="Distribuição dos dados (apenas um exemplo genérico)"
        )
        graph_div = plotly.offline.plot(fig, auto_open=False, output_type="div")
        return graph_div


    def dist_text(self):
        distinct = ", ".join([str(x) for x in set(self.data)][0:10])
        div = f"<div>{distinct}</div>"
        return div

