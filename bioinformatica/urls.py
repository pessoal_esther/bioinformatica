from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

from bioinformatica import settings
from website.views import home

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('website.urls')),
    path('accounts/', include('django.contrib.auth.urls'))
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
